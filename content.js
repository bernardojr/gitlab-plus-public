/***
 * Returns the name of the last branch pushed.
 * It is parsed from the "You pushed [BRANCH] about [TIME] ago"
 * @returns {string}
 */
function getLastBranchPushed() {
    var aLastBranchPushed = document.querySelectorAll('.event-last-push-text .ref-name');
    if (aLastBranchPushed.length === 1
        && aLastBranchPushed[0].href.startsWith('https://gitlab.com/USER/REPO')
        && (aLastBranchPushed[0].text.startsWith('master-'))
            || aLastBranchPushed[0].text.startsWith('development-')
            || aLastBranchPushed[0].text.startsWith('dev-')
    ) {
        return aLastBranchPushed[0].text;
    }

    console.info('Cannot parse the last branch pushed');
    return false;
}

/***
 * Returns the button DOM for "Create merge request"
 * @returns {button}
 */
function getButtonDefaultCreateMergeRequest() {
    var btnCreateNewMergeRequest = document.querySelectorAll('.event-last-push .pull-right .btn.btn-info.btn-sm');
    if (btnCreateNewMergeRequest.length === 1
        && btnCreateNewMergeRequest[0].title === 'New merge request'
        && btnCreateNewMergeRequest[0].href.startsWith('https://gitlab.com/USER/REPO/merge_requests/new?')
    ) {
        return btnCreateNewMergeRequest[0];
    }

    console.info('Cannot parse the default merge button');
    return false;
}

/***
 * Main runner function
 * @returns {boolean}
 */
function run() {
    var lastBranchPushed = getLastBranchPushed();
    var btnDefaultCreateMergeRequest = getButtonDefaultCreateMergeRequest();
    if (lastBranchPushed === false || btnDefaultCreateMergeRequest === false) {
        return false;
    }

    var targetBranch = '';
    if (lastBranchPushed.startsWith('master-')) {
        targetBranch = 'master';
    } else if (lastBranchPushed.startsWith('development-')|| lastBranchPushed.startsWith('dev-')) {
        targetBranch = 'development';
    } else {
        console.info('Cannot decide on target branch using: ' + lastBranchPushed);
        return false;
    }

    var newMergeUrl = btnDefaultCreateMergeRequest.href.replace('&merge_request%5Btarget_branch%5D=master', '&merge_request%5Btarget_branch%5D='+targetBranch);
    var btnMergeInto = document.createElement('a')
    btnMergeInto.className = 'btn btn-info btn-sm';
    btnMergeInto.title = 'Request merge ' + lastBranchPushed + ' Into ' + targetBranch;
    btnMergeInto.style.marginRight = '5px';
    btnMergeInto.style.backgroundColor = '#ff9100';
    btnMergeInto.style.borderColor = '#ff6d00';
    btnMergeInto.href = newMergeUrl;
    btnMergeInto.innerHTML = 'Request merge <b>' + lastBranchPushed + '</b> into <b>' + targetBranch + '</b>';
    btnDefaultCreateMergeRequest.parentNode.insertBefore(btnMergeInto, btnDefaultCreateMergeRequest);

    console.info('Successfully added button.');
}

run();